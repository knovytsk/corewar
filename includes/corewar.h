/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: knovytsk <knovytsk@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/08 16:59:07 by knovytsk          #+#    #+#             */
/*   Updated: 2018/06/08 16:59:08 by knovytsk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COREWAR_H
# define COREWAR_H

# include <stdio.h>

# include "op.h"
# include "libft.h"
# include "ncurses.h"
# include <fcntl.h>

# define NAME 0

//for array of structures (for 16 operations)
enum 					e_operations
{
	Live = 1,
	Ld = 2,
	St = 3,
	Add = 4,
	Sub = 5,
	And = 6,
	Or = 7,
	Xor = 8,
	Zjmp = 9,
	Ldi = 10,
	Sti = 11,
	Fork = 12,
	Lld = 13,
	Lldi = 14,
	Lfork = 15,
	Aff = 16
}						t_operations;

//description of one operation
typedef	struct 			s_operation
{
	char				carry;
	char				codage;
	char				label_size;
	int 				cycles;
	//operation
	// void 				(*execute)(unsigned char *map, struct s_operation *op, int pc);
}						t_operation;

//one list of processes for the game
typedef	struct 			s_process
{
	int 				pc;
	unsigned int		registry[16];
	int 				live;
	int 				counter;
	int 				carry;
	unsigned char 		op;
	int 				pos_changed;
	//t_operation			operation;
	void 				(*execute)(unsigned char *map, struct s_process *p);
	struct s_process	*next;
}						t_process;

typedef	struct 			s_bot
{
	header_t 			prog;
	unsigned int 		start;
	unsigned char 		*exec_code;
}						t_bot;

typedef void 			(*t_op)(unsigned char *map, t_process *p);

//GENERAL STRUCTURE
typedef struct 			s_vm
{
	t_process 			*proc;
	t_bot				bot[MAX_PLAYERS];
	t_op 				op[17];
	int 				cycles[17];
	// t_operation			op[17];
	unsigned char 		map[MEM_SIZE];
	int 				cycle;
	int 				players_num;
	int 				visual_mode;
	int 				run;
}						t_vm;

//parse_bot
int    					parse_bot(t_bot *bot, char *file);

//game_init
void    				game_init(t_vm *vm);

//operations_init
void    				operations_init(t_vm *vm);

//process
void    				add_process(t_vm *vm, int pc, int bot);

//visual
void    				visual_init(t_vm *vm);

//OPERATIONS
// void 					add(unsigned char *map, t_operation *op, int pc);
// void 					aff(unsigned char *map, t_operation *op, int pc);
// void 					and(unsigned char *map, t_operation *op, int pc);
// void 					frk(unsigned char *map, t_operation *op, int pc);
// void 					ld(unsigned char *map, t_operation *op, int pc);
// void 					ldi(unsigned char *map, t_operation *op, int pc);
// void 					lfork(unsigned char *map, t_operation *op, int pc);
// void 					live(unsigned char *map, t_operation *op, int pc);
// void 					lld(unsigned char *map, t_operation *op, int pc);
// void 					lldi(unsigned char *map, t_operation *op, int pc);
// void 					or(unsigned char *map, t_operation *op, int pc);
// void 					st(unsigned char *map, t_operation *op, int pc);
// void 					sti(unsigned char *map, t_operation *op, int pc);
// void 					sub(unsigned char *map, t_operation *op, int pc);
// void 					xor(unsigned char *map, t_operation *op, int pc);
// void 					zjmp(unsigned char *map, t_operation *op, int pc);

void 					add(unsigned char *map, t_process *p);
void 					aff(unsigned char *map, t_process *p);
void 					and(unsigned char *map, t_process *p);
void 					frk(unsigned char *map, t_process *p);
void 					ld(unsigned char *map, t_process *p);
void 					ldi(unsigned char *map, t_process *p);
void 					lfork(unsigned char *map, t_process *p);
void 					live(unsigned char *map, t_process *p);
void 					lld(unsigned char *map, t_process *p);
void 					lldi(unsigned char *map, t_process *p);
void 					or(unsigned char *map, t_process *p);
void 					st(unsigned char *map, t_process *p);
void 					sti(unsigned char *map, t_process *p);
void 					sub(unsigned char *map, t_process *p);
void 					xor(unsigned char *map, t_process *p);
void 					zjmp(unsigned char *map, t_process *p);


//TO DELETE
void    				print_bot(t_bot *bot);
void    				print_map(t_vm *vm);
void    				print_args(unsigned char *arg_type, unsigned int *arg, int n);
void    				print_process(t_process *p);
//END

#endif