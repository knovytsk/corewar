# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: knovytsk <knovytsk@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/06/08 16:47:58 by knovytsk          #+#    #+#              #
#    Updated: 2018/06/08 16:48:00 by knovytsk         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = corewar

#	C compiler and his default flags
GCC = gcc -Wall -Wextra -Werror
FLAGS = -lncurses
#directories
SRCDIR = ./srcs/
OPERATIONDIR = ./srcs/operations/
VISUALDIR = ./visual/
INCLUDE = ./includes
LIBFTINCLUDE = ./libft
VISUALINCLUDE = ./visual/includes
BINDIR = ./objs/

#	Source files
SRCFILES = main.c parse_bot.c game_init.c to_delete.c process.c operations_init.c 
OPERATIONFILES = live.c ld.c st.c add.c sub.c and.c or.c xor.c zjmp.c ldi.c sti.c fork.c lld.c lldi.c lfork.c aff.c
VISUALFILES = visual_init.c map.c info.c cycle.c processes.c
#	Binaries list
BIN = $(addprefix $(BINDIR), $(SRCFILES:.c=.o)) \
	  $(addprefix $(BINDIR), $(VISUALFILES:.c=.o)) \
	  $(addprefix $(BINDIR), $(OPERATIONFILES:.c=.o))
#	Libft
LIBFT = ./libft/libft.a

HEADER_RELATION = ./includes/corewar.h ./includes/op.h ./visual/includes/visual.h

.NOTPARALLEL = all fclean clean re $(NAME)

all: $(LIBFT) $(NAME)

$(NAME): $(BINDIR) $(BIN)
	@$(GCC) -o $(NAME) $(BIN) $(LIBFT) $(FLAGS)

$(BINDIR):
	@if [ ! -d "$(BINDIR)" ]; then mkdir $(BINDIR); fi

$(BINDIR)%.o: $(SRCDIR)%.c $(HEADER_RELATION)
	@$(GCC) -c -I $(INCLUDE) -I $(LIBFTINCLUDE) $< -o $@

$(BINDIR)%.o: $(OPERATIONDIR)%.c $(HEADER_RELATION)
	@$(GCC) -c -I $(INCLUDE) -I $(LIBFTINCLUDE) $< -o $@

$(BINDIR)%.o: $(VISUALDIR)%.c $(HEADER_RELATION)
	@$(GCC) -c -I $(INCLUDE) -I $(LIBFTINCLUDE) -I $(VISUALINCLUDE) $< -o $@

clean:
	@make -C ./libft/ clean
	@if [ -d "$(BINDIR)" ]; then rm -rf $(BINDIR); fi
	@echo "\033[32;1mCleaned\033[0m"

fclean: clean
	@make -C ./libft/ fclean
	@if [ -f "$(NAME)" ]; then rm -rf $(NAME); fi

re: fclean all

$(LIBFT):
	@echo "\033[33;1mCompilation of libft...\033[0m"
	@make -C ./libft/
