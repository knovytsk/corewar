/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: knovytsk <knovytsk@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/08 16:59:07 by knovytsk          #+#    #+#             */
/*   Updated: 2018/06/08 16:59:08 by knovytsk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visual.h"

void    execute(t_vm *vm, t_process *p, int bot)
{
    if (p->op == Live)
    {
        attron(A_BOLD | COLOR_PAIR(bot + LIVE_COLOR));
        mvprintw(Y(p->pc), X(p->pc), "%02x", p->op);
        attroff(A_BOLD | COLOR_PAIR(bot + LIVE_COLOR));
    }
    p->counter = 0;
    vm->op[p->op](vm->map, p);
    draw_processes(vm);
}

void    cycle(t_vm *vm)
{
    t_process   *p;
    int         bot;

    vm->cycle++;
    p = vm->proc;
    while (p)
    {
        bot = (p->registry[0] * (-1)) - 1;
        p->counter++;
        if (p->counter == vm->cycles[p->op])
            execute(vm, p, bot);
        p = p->next;
    }
    attron(A_BOLD);
    mvprintw(2, MAP_WIDTH + 7, "CYCLE :   ");  
    mvprintw(2, MAP_WIDTH + 7, "CYCLE : %i", vm->cycle);
    attroff(A_BOLD);
    if (!vm->proc)
    {
        //show results
        return  ;
    }
}

int    run(t_vm *vm, t_win *win)
{
    int     input;

    while (++vm->cycle < 2000)
    {
        usleep(40000);
        cycle(vm);
        check_resize(win, vm);
        input = getch();
        if (input == 'q')
            return (0);
        if (input == ' ')
            return (1);
        if (!vm->proc)
        {
            //show results
            return (1);
        }
    }
    return (1);
}