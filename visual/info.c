/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: knovytsk <knovytsk@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/08 16:59:07 by knovytsk          #+#    #+#             */
/*   Updated: 2018/06/08 16:59:08 by knovytsk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visual.h"

void    usage_info()
{
    int x;
    int y;

    y = HEIGHT * 0.7;
    x = MAP_WIDTH + 7;
    mvprintw(y + 2, x, "NEXT CYCLE : ");
    mvprintw(y + 4, x, "RUN : ");
    mvprintw(y + 6, x, "QUIT : ");
    // attron(A_BOLD | COLOR_PAIR(3));
    mvprintw(y + 2, x + 13, "S");
    mvprintw(y + 4, x + 6, "SPACE");
    mvprintw(y + 6, x + 7, "Q");
    // attroff(A_BOLD | COLOR_PAIR(3));
}

void    bots(t_vm *vm)
{
    int num;
    int y;
    int x;

    num = -1;
    y = HEIGHT * 0.25;
    x = MAP_WIDTH + 7;
    while (++num < vm->players_num)
    {
        mvprintw(y + num * 2, x, "Player : ");
        attron(COLOR_PAIR(num + BOT_COLOR));
        mvprintw(y + num * 2, x + 9, "%s", vm->bot[num].prog.prog_name);
        attroff(COLOR_PAIR(num + BOT_COLOR));
    }
}

void    draw_info(t_win *win, t_vm *vm)
{
    int y;
    int x;

    (void)win;
    y = HEIGHT * 0.5 + 2;
    x = MAP_WIDTH + 7;
    attron(A_BOLD);
    mvprintw(y, x, "CYCLE_TO_DIE : %i", CYCLE_TO_DIE);
    mvprintw(y + 2, x, "CYCLE_DELTA :  %i", CYCLE_DELTA);
    mvprintw(y + 4, x, "NBR_LIVE :     %i", NBR_LIVE);
    mvprintw(y + 6, x, "MAX_CHECKS :   %i", MAX_CHECKS);
    bots(vm);
    mvprintw(2, MAP_WIDTH + 7, "CYCLE :   ");  
    mvprintw(2, MAP_WIDTH + 7, "CYCLE : %i", vm->cycle);
    usage_info();
    refresh();
    attroff(A_BOLD);
}