/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: knovytsk <knovytsk@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/08 16:59:07 by knovytsk          #+#    #+#             */
/*   Updated: 2018/06/08 16:59:08 by knovytsk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visual.h"

void    draw_borders(t_win *win)
{
    wrefresh(win->my_win);
    attron(COLOR_PAIR(1));
    mvhline(0, 0, 0, WIDTH);
    mvhline(HEIGHT, 0, 0, WIDTH);
    mvvline(0, 0, 0, HEIGHT);
    mvvline(0, WIDTH, 0, HEIGHT + 1);
    mvvline(0, MAP_WIDTH + 4, 0, HEIGHT);
    mvhline(HEIGHT * 0.5, MAP_WIDTH + 4, 0, WIDTH - MAP_WIDTH - 4);
    mvhline(HEIGHT * 0.7, MAP_WIDTH + 4, 0, WIDTH - MAP_WIDTH - 4);
    mvhline(HEIGHT * 0.2, MAP_WIDTH + 4, 0, WIDTH - MAP_WIDTH - 4);
    attroff(COLOR_PAIR(1));
    refresh();
}

void    init_pairs()
{
    init_color(COLOR_WHITE, 500, 500, 500);
    init_pair(1, COLOR_WHITE, COLOR_WHITE);
    init_pair(2, COLOR_GREEN, COLOR_BLACK);
    init_pair(3, COLOR_YELLOW, COLOR_BLACK);
    init_pair(4, COLOR_CYAN, COLOR_BLACK);
    init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(6, COLOR_BLACK, COLOR_GREEN);
    init_pair(7, COLOR_BLACK, COLOR_YELLOW);
    init_pair(8, COLOR_BLACK, COLOR_CYAN);
    init_pair(9, COLOR_BLACK, COLOR_MAGENTA);
    init_pair(10, COLOR_WHITE, COLOR_GREEN);
    init_pair(11, COLOR_WHITE, COLOR_YELLOW);
    init_pair(12, COLOR_WHITE, COLOR_CYAN);
    init_pair(13, COLOR_WHITE, COLOR_MAGENTA);
}

void    init_curs()
{
    initscr();
    start_color(); 
    cbreak();
    keypad(stdscr, TRUE);
    noecho();
    curs_set(false);
    nodelay(stdscr, true);
    init_pairs();
}

void    draw(t_win *win, t_vm *vm)
{
    ioctl(0, TIOCGWINSZ, &win->size);
    delwin(win->my_win);
    win->my_win = newwin(win->size.ws_row, win->size.ws_col, 0, 0);
    draw_borders(win);
    draw_map(win, vm);
    draw_info(win, vm);
}

void    check_resize(t_win *win, t_vm *vm)
{
    int w;
    int h;

    w = win->size.ws_col;
    h = win->size.ws_row;
    ioctl(0, TIOCGWINSZ, &win->size);
    if (h != win->size.ws_row || w != win->size.ws_col)
        draw(win, vm);
}

void    visual_init(t_vm *vm)
{
    t_win   win;
    int     input;

    input = 0;
    init_curs();
    draw(&win, vm);
    vm->cycle = 0;
    win.prev = 0;
    while (input != 'q')
    {
        check_resize(&win, vm);
        input = getch();
        if (input == 's')
        {
            cycle(vm);
            draw_info(&win, vm);
        }
        if (input == ' ')
        {
            if (!run(vm, &win))
                break ;
        }
    }
    endwin();
}
