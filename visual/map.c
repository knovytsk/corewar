/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: knovytsk <knovytsk@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/08 16:59:07 by knovytsk          #+#    #+#             */
/*   Updated: 2018/06/08 16:59:08 by knovytsk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visual.h"

void    draw_map(t_win *win, t_vm *vm)
{
    unsigned int    i;
    int             num;

    i = -1;
    num = 0;
    (void)win;
    while (++i < MEM_SIZE)
    {
        if (num < vm->players_num)
        {
            if (i == vm->bot[num].start)
                attron(COLOR_PAIR(num + BOT_COLOR));
        }
        mvprintw(Y(i), X(i), "%02x", vm->map[i]);
        if (i - vm->bot[num].start == vm->bot[num].prog.prog_size - 1)
        {
            attroff(COLOR_PAIR(num + BOT_COLOR));
            num++;
        }
    }
    draw_processes(vm);
    refresh();
}