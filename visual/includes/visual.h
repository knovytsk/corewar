/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visual.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: knovytsk <knovytsk@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/08 16:59:07 by knovytsk          #+#    #+#             */
/*   Updated: 2018/06/08 16:59:08 by knovytsk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VISUAL_H
# define VISUAL_H

#include "corewar.h"
# include <sys/ioctl.h>

# define MAP_WIDTH  ((MEM_SIZE / 64) * 3)
# define WIDTH      (MAP_WIDTH + MAP_WIDTH * 0.3)
# define HEIGHT     (MEM_SIZE / 64 + 3)

# define X(x)       (((x % 64) * 3) + 3)
# define Y(y)       ((int)(y / 64) + 2)

# define LIVE_CYCLE 30 
# define REG_CYCLE 50

# define BOT_COLOR 2
# define PC_COLOR 6
# define LIVE_COLOR 10

typedef struct      s_win
{
    WINDOW          *my_win;
    struct winsize  size;
    int             prev;
    int             first_reg[4];
    int             first_live[4];
    int             f_reg[4];
    int             f_live[4];
}                   t_win;

void                draw_map(t_win *win, t_vm *vm);
void                draw_info(t_win *win, t_vm *vm);
void                draw_processes(t_vm *vm);

void                cycle(t_vm *vm);
int                 run(t_vm *vm, t_win *win);

void                check_resize(t_win *win, t_vm *vm);

#endif