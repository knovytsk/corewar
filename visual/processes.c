/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   processes.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: knovytsk <knovytsk@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/08 16:59:07 by knovytsk          #+#    #+#             */
/*   Updated: 2018/06/08 16:59:08 by knovytsk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visual.h"

void    change_reg(t_vm *vm, t_process *p, int bot)
{
    int i;
    int pos;

    attron(A_BOLD | COLOR_PAIR(bot + BOT_COLOR));
    i = -1;
    while (++i < 4)
    {
        pos = p->pos_changed + i;
        // printf("pos->%i %i\n", Y(pos), X(pos));
        mvprintw(Y(pos), X(pos), "%02x", vm->map[pos]);
    }
    attroff(A_BOLD | COLOR_PAIR(bot + BOT_COLOR));
}

void    draw_processes(t_vm *vm)
{
    static int  prev[4] = {-1, -1, -1, -1};
    t_process   *p;
    int         bot;

    p = vm->proc;
    while (p)
    {
        bot = (p->registry[0] * (-1)) - 1;
        if (p->pos_changed != 0)
            change_reg(vm, p, bot);
        if (prev[bot] != -1 && p->live < 1)
        {
            attron(COLOR_PAIR(bot + BOT_COLOR));
            mvprintw(Y(prev[bot]), X(prev[bot]), "%02x", vm->map[prev[bot]]);
            attroff(COLOR_PAIR(bot + BOT_COLOR));
        }
        attron(COLOR_PAIR(bot + PC_COLOR));
        mvprintw(Y(p->pc), X(p->pc), "%02x", p->op);
        attroff(COLOR_PAIR(bot + PC_COLOR));
        prev[bot] = p->pc;
        p = p->next;
    }
    refresh();
}