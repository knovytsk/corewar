/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: knovytsk <knovytsk@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/08 16:59:07 by knovytsk          #+#    #+#             */
/*   Updated: 2018/06/08 16:59:08 by knovytsk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

t_process *create_process(t_vm *vm, int pc, int bot)
{
    t_process *new_proc;
    int i;

    new_proc = (t_process*)malloc(sizeof(t_process));
    i = -1;
    while (++i < 16)
        new_proc->registry[i] = 0;
    new_proc->pc = pc;
    new_proc->op = vm->map[pc];
    new_proc->carry = 1;
    new_proc->registry[NAME] = -(bot);
    new_proc->live = 0;
    new_proc->counter = 0;
    new_proc->pos_changed = 0;
    // if (vm->map[pc] != 0)
    //     new_proc->operation = vm->op[(int)vm->map[pc]];
    new_proc->next = NULL;
    return (new_proc);
}

void    add_process_to_list(t_process **p, t_process *new)
{
    if (p == NULL)
	{
		*p = new;
		new->next = NULL;
	}
	new->next = *p;
	*p = new;
}

void    add_process(t_vm *vm, int pc, int bot)
{
    if (!vm->proc)
        vm->proc = create_process(vm, pc, bot);
    else
        add_process_to_list(&vm->proc, create_process(vm, pc, bot));
}