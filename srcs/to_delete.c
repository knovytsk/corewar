
#include "corewar.h"

void    print_bot(t_bot *bot)
{
    printf("MAGIC: %#x\n", bot->prog.magic);
    int j = -1;
    printf("NAME: ");
    while (++j < PROG_NAME_LENGTH)
        printf("%c", bot->prog.prog_name[j]);
    printf("\n");
    printf("SIZE: %u\n", bot->prog.prog_size);
    j = -1;
    printf("COMMENT: ");
    while (++j < COMMENT_LENGTH)
        printf("%c", bot->prog.comment[j]);
    printf("\n");
    printf("CODE: ");
    j = -1;
    while (++j < (int)bot->prog.prog_size)
    {
        printf("%02x ", bot->exec_code[j]);  
        if (j % 16 == 0)
            printf("\n");
    }  
    printf("\n");
}

void    print_map(t_vm *vm)
{
    int line;
    int    n;
    unsigned int i;
    unsigned int j;

    line = 0;
    i = -1;
    n = 0;
    while (++i < MEM_SIZE)
    {
        if (i == vm->bot[n].start)
        {
            j = -1;
            while (++j < vm->bot[n].prog.prog_size)
            {
                ft_printf("{green}%02x {eoc}", vm->map[i]);
                i++;
                line++;
                if (line == 64)
                {
                    ft_printf("\n");
                    line = 0;
                }
            }
            n++;
        }
        ft_printf("%02x ", vm->map[i]);
        line++;
        if (line == 64)
        {
            ft_printf("\n");
            line = 0;
        }
    }

}

void    print_args(unsigned char *arg_type, unsigned int *arg, int n)
{
    int i;

    i = -1;
    while (++i < n)
    {
        if (arg_type[i] == DIR_CODE)
            printf("[%i]->T_DIR\n", i + 1);
        else if (arg_type[i] == IND_CODE)
            printf("[%i]->T_IND\n", i + 1);
        else if (arg_type[i] == REG_CODE)
            printf("[%i]->T_REG\n", i + 1);
    }
    i = -1;
    while (++i < n)
    {
        printf("%#02x\n", arg[i]);
    }
}

void    print_process(t_process *p)
{
    printf("\n=====================================\n");
    printf("PC: %i\n", p->pc);
    int i = -1;
    printf("REGISTRY: ");
    while (++i < 16)
        printf("%i ", p->registry[i]);
    printf("\n");
    printf("LIVE: %i\n", p->live);
    printf("COUNTER: %i\n", p->counter);
    printf("CARRY: %i\n", p->carry);
    printf("CHANGED: %i\n", p->pos_changed);
    printf("\n======================================\n");
}