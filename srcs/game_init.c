/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: knovytsk <knovytsk@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/08 16:59:07 by knovytsk          #+#    #+#             */
/*   Updated: 2018/06/08 16:59:08 by knovytsk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

void    run_game(t_vm *vm)
{
    t_process *t;
    
    vm->cycle = -1;
    while (++vm->cycle < 50)
    {
        t = vm->proc;
        while (t)
        {
            if (t->counter == vm->cycles[t->op])
            {
                t->counter = 0;
                vm->op[t->op](vm->map, t);
            }
            t->counter++;    
            t = t->next;
        }
        // break ;
        if (!vm->proc)
        {
            //show results
            break ;
        }
    }
}

void    game_init(t_vm *vm)
{
    int             n;
    unsigned int    i;
    unsigned int    j;
    int             space;

    n = -1;
    ft_bzero(vm->map, MEM_SIZE);
    space = MEM_SIZE / vm->players_num;
    while (++n < vm->players_num)
    {
        vm->bot[n].start = n * space;
        i = vm->bot[n].start;
        j = 0;
        while (j < vm->bot[n].prog.prog_size)
            vm->map[i++] = vm->bot[n].exec_code[j++];
    }
    n = -1;
    while (++n < vm->players_num)
        add_process(vm, vm->bot[n].start, n + 1);
    if (!vm->visual_mode)
        run_game(vm);
    // print_map(vm);
}