/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   operations_init.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: knovytsk <knovytsk@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/08 16:59:07 by knovytsk          #+#    #+#             */
/*   Updated: 2018/06/08 16:59:08 by knovytsk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

void    init_carry(t_operation *op)
{
    op[Live].carry = 0;
    op[Ld].carry = 1;
    op[St].carry = 0;
    op[Add].carry = 1;
    op[Sub].carry = 1;
    op[And].carry = 1;
    op[Or].carry = 1;
    op[Xor].carry = 1;
    op[Zjmp].carry = 0;
    op[Ldi].carry = 0;
    op[Sti].carry = 0;
    op[Fork].carry = 0;
    op[Lld].carry = 1;
    op[Lldi].carry = 1;
    op[Lfork].carry = 0;
    op[Aff].carry = 0;
}

void    init_codage(t_operation *op)
{
    op[Live].codage = 0; 
    op[Ld].codage = 1;
    op[St].codage = 1;
    op[Add].codage = 1;
    op[Sub].codage = 1;
    op[And].codage = 1;
    op[Or].codage = 1;
    op[Xor].codage = 1;
    op[Zjmp].codage = 0;
    op[Ldi].codage = 1;
    op[Sti].codage = 1;
    op[Fork].codage = 0;
    op[Lld].codage = 1;
    op[Lldi].codage = 1;
    op[Lfork].codage = 0;
    op[Aff].codage = 1;
}

void    init_label_size(t_operation *op)
{
    op[Live].label_size = 4;
    op[Ld].label_size = 4;
    op[St].label_size = 4;
    op[Add].label_size = 4;
    op[Sub].label_size = 4;
    op[And].label_size = 4;
    op[Or].label_size = 4;
    op[Xor].label_size = 4;
    op[Zjmp].label_size = 2;
    op[Ldi].label_size = 2;
    op[Sti].label_size = 2;
    op[Fork].label_size = 2;
    op[Lld].label_size = 4;
    op[Lldi].label_size = 2;
    op[Lfork].label_size = 2;
    op[Aff].label_size = 4;
}

void    init_cycles(t_operation *op)
{
    op[Live].cycles = 10;
    op[Ld].cycles = 5;
    op[St].cycles = 5;
    op[Add].cycles = 10;
    op[Sub].cycles = 10;
    op[And].cycles = 6;
    op[Or].cycles = 6;
    op[Xor].cycles = 6;
    op[Zjmp].cycles = 20;
    op[Ldi].cycles = 25;
    op[Sti].cycles = 25;
    op[Fork].cycles = 800;
    op[Lld].cycles = 10;
    op[Lldi].cycles = 50;
    op[Lfork].cycles = 1000;
    op[Aff].cycles = 2;
}

void    operations_init(t_vm *vm)
{
    // init_carry(vm->op);
    // init_codage(vm->op);
    // init_label_size(vm->op);
    // init_cycles(vm->op);
    // vm->op[Live].execute = &live;;
    // vm->op[Ld].execute = &ld;
    // vm->op[St].execute = &st;
    // vm->op[Add].execute = &add;
    // vm->op[Sub].execute = &sub;
    // vm->op[And].execute = &and;
    // vm->op[Or].execute = &or;
    // vm->op[Xor].execute = &xor;
    // vm->op[Zjmp].execute = &zjmp;
    // vm->op[Ldi].execute = &ldi;
    // vm->op[Sti].execute = &sti;
    // vm->op[Fork].execute = &frk;
    // vm->op[Lld].execute = &lld;
    // vm->op[Lldi].execute = &lldi;
    // vm->op[Lfork].execute = &lfork;
    // vm->op[Aff].execute = &aff;

    vm->op[Live] = &live;;
    vm->op[Ld] = &ld;
    vm->op[St] = &st;
    vm->op[Add] = &add;
    vm->op[Sub] = &sub;
    vm->op[And] = &and;
    vm->op[Or] = &or;
    vm->op[Xor] = &xor;
    vm->op[Zjmp] = &zjmp;
    vm->op[Ldi] = &ldi;
    vm->op[Sti] = &sti;
    vm->op[Fork] = &frk;
    vm->op[Lld] = &lld;
    vm->op[Lldi] = &lldi;
    vm->op[Lfork] = &lfork;
    vm->op[Aff] = &aff;

    vm->cycles[Live] = 10;
    vm->cycles[Ld] = 5;
    vm->cycles[St] = 5;
    vm->cycles[Add] = 10;
    vm->cycles[Sub] = 10;
    vm->cycles[And] = 6;
    vm->cycles[Or] = 6;
    vm->cycles[Xor] = 6;
    vm->cycles[Zjmp] = 20;
    vm->cycles[Ldi] = 25;
    vm->cycles[Sti] = 25;
    vm->cycles[Fork] = 800;
    vm->cycles[Lld] = 10;
    vm->cycles[Lldi] = 50;
    vm->cycles[Lfork] = 1000;
    vm->cycles[Aff] = 2;
}