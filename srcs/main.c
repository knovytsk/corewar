/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: knovytsk <knovytsk@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/08 16:59:07 by knovytsk          #+#    #+#             */
/*   Updated: 2018/06/08 16:59:08 by knovytsk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

void    flag_analyze(t_vm *vm, char *av)
{
    if (ft_strequ(av, "-v"))
        vm->visual_mode = 1;
}

void    check_args(t_vm *vm, int ac, char **av)
{
    int i;
    int num;

    i = 0;
    num = 0;
    while (++i < ac)
    {
        if (ft_strstr(av[i], ".cor"))
        {
            if (parse_bot(&vm->bot[num], av[i]))
                num++;
        }
        else
            flag_analyze(vm, av[i]);
        if (num > 4)
            error_exit(0, "Players amount is exceeded.");
    }
    vm->players_num = num;
}

int     main(int ac, char **av)
{
    t_vm    vm;

    // vm = (t_vm){NULL, 0, {0, 0, 0}, 0, 0, 0, 1};
    if (ac == 1)
        error_exit(0, "Usage: ./corewar <champion1.cor> ... <champion4.cor>");
    vm.visual_mode = 0;
    vm.proc = NULL;
    vm.run = 1;
    check_args(&vm, ac, av);
    operations_init(&vm);
    game_init(&vm);
    if (vm.visual_mode)
        visual_init(&vm);
    return (0);
}