/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: knovytsk <knovytsk@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/08 16:59:07 by knovytsk          #+#    #+#             */
/*   Updated: 2018/06/08 16:59:08 by knovytsk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

void    parse_bot2(int fd, t_bot *bot, char *file)
{
    unsigned int    j;
    unsigned char   buf[4];

    j = 0;
    while (j < COMMENT_LENGTH && read(fd, buf, 1))
        bot->prog.comment[j++] = buf[0];
    if (j < COMMENT_LENGTH)
        error_file(1, file, "Incorrect file structure: COMMENT_LENGTH");
    bot->exec_code = (unsigned char*)malloc(sizeof(unsigned char) *
                                        (bot->prog.prog_size + 1));
    j = 0;
    read(fd, buf, 4);
    while (j < bot->prog.prog_size && read(fd, buf, 1))
        bot->exec_code[j++] = buf[0];
    if (j != bot->prog.prog_size)
        error_file(1, file, "Incorrect size of executable code.");
}

int    parse_bot(t_bot *bot, char *file)
{
    int             fd;
    unsigned int    j;
    unsigned char   buf[4];

    fd = open(file, O_RDONLY);
    j = 0;
    read(fd, buf, 4);
    bot->prog.magic = (buf[1] << 16 | buf[2] << 8 | buf[3]);
    if (bot->prog.magic != COREWAR_EXEC_MAGIC)
        error_file(1, file, "Magic number is wrong.");
    j = 0;
    while (j < PROG_NAME_LENGTH && read(fd, buf, 1))
        bot->prog.prog_name[j++] = buf[0];
    if (j < PROG_NAME_LENGTH)
        error_file(1, file, "Incorrect file structure: PROG_NAME_LENGTH.");
    read(fd, buf, 4);
    read(fd, buf, 4);
    bot->prog.prog_size = (buf[1] << 16 | buf[2] << 8 | buf[3]);
    if (bot->prog.prog_size >= CHAMP_MAX_SIZE)
        error_file(1, file, "Size of champion is too big.");
    parse_bot2(fd, bot, file);
    close(fd);
    
    // print_bot(bot);
    return (1);
}