
#include "corewar.h"

void st(unsigned char *map, t_process *p)
{
    // printf("--------------st-----------------\n");
    (void)map;
    (void)p;

    unsigned char   arg_type[2];
    unsigned int    arg[2];  
    unsigned char   encode;
    int             pos;

    encode = map[p->pc + 1];
    arg_type[0] = encode >> 6;
    arg_type[1] = (encode >> 4) & 0x3;
    arg[0] = map[p->pc + 2];
    if (arg_type[1] == IND_CODE)
    {
        arg[1] = (map[p->pc + 3] >> 8 | map[p->pc + 4]);
        pos = p->pc + (arg[1] % IDX_MOD);
        map[pos] = (p->registry[0] >> 24 & 0xFF);
        map[pos + 1] = (p->registry[0] >> 16 & 0xFF);
        map[pos + 2] = (p->registry[0] >> 8 & 0xFF);
        map[pos + 3] = (p->registry[0] & 0xFF);
        p->pos_changed = pos;
        p->pc += (REG_SIZE + IND_SIZE + 2);
    }
    else if (arg_type[1] == REG_CODE)
    {
        arg[1] = map[p->pc + 3];
        p->registry[arg[1]] = p->registry[arg[0]];
        p->pc += (REG_SIZE + REG_SIZE + 2);
    }
    p->op = map[p->pc];
    // print_args(arg_type, arg, 2);
    // print_process(p);
}