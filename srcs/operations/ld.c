
#include "corewar.h"

void ld(unsigned char *map, t_process *p)
{
    // printf("--------------ld-----------------\n");
    unsigned char   arg_type[2];
    unsigned int    arg[2];  
    unsigned char   encode;
    int             pos;

    encode = 0;
    pos = 0;
    encode = map[p->pc + 1];
    arg_type[0] = encode >> 6;
    arg_type[1] = (encode >> 4) & 0x3;
    if (arg_type[0] == DIR_CODE)
    {
        arg[0] = (map[p->pc + 3] << 16 | map[p->pc + 4] << 8 | map[p->pc + 5]);
        arg[1] = map[p->pc + 6];
        p->registry[arg[1]] = arg[0];
        p->pc += (DIR_SIZE + REG_SIZE + 2);
    }
    else if (arg_type[0] == IND_CODE)
    {
        arg[0] = (arg[0] % IDX_MOD);
        pos = p->pc + arg[0];
        p->registry[arg[1]] = (map[pos + 3] << 16 | map[pos + 4] << 8 | map[pos + 5]);
        if (p->registry[arg[1]] == 0)
            p->carry = 1;
        else
            p->carry = 0;
        p->pc += (IND_SIZE + REG_SIZE + 2);
    }
    p->op = map[p->pc];
    // print_args(arg_type, arg, 2);
    // print_process(p);
}