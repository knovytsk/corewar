
#include "corewar.h"

void live(unsigned char *map, t_process *p)
{
    // printf("--------------live-----------------\n");
    unsigned int arg;

    arg = (map[p->pc + 1] << 24 | map[p->pc + 2] << 16 | map[p->pc + 3] << 8 | map[p->pc + 4]);
    // p->live++;
    if (arg == p->registry[0])
        p->live++;
    p->pc += DIR_SIZE + 1;
}